package org.inventory.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.inventory.dao.util.DbConnectionUtil;
import org.inventory.jdo.User;

public class UserDao {

	
	public static User validateUser(String username, String password)
	{
		User retUser = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query = "SELECT FIRST_NAME, MIDDLE_NAME, LAST_NAME, USER_NAME, ";
		query += "USER_TYPE FROM USERS WHERE USER_NAME = ? AND USER_PASSWORD = ?";
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
			pst.setString(1, username);
			pst.setString(2, password);
			
			rs = pst.executeQuery();
			
			while(rs.next())
			{
				retUser = new User();
				retUser.setFirstName(rs.getString(1));
				retUser.setMiddleName(rs.getString(2));
				retUser.setLastName(rs.getString(3));
				retUser.setUserName(rs.getString(4));
				retUser.setUserType(rs.getString(5));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return retUser;
	}
	
	public static boolean saveUser(String userName, String userPassword, 
		String userType, String firstName, String middleName, String lastName)
	{
		boolean status = false;
		Connection con = null;
		PreparedStatement pst = null;
		String query = "INSERT INTO USERS (USER_NAME, USER_PASSWORD, USER_TYPE, ";
			query += "FIRST_NAME, MIDDLE_NAME, LAST_NAME ) VALUES (?,?,?,?,?,?)";
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
			pst.setString(1, userName);
			pst.setString(2, userPassword);
			pst.setString(3, userType);
			pst.setString(4, firstName);
			pst.setString(5, middleName);
			pst.setString(6, lastName);
			
			int rows = pst.executeUpdate();
			if(rows > 0)
			{
				status = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return status;
	}
	
	/*
	
	public static boolean updateCourseDetail(String course, String sp, String fa, int sections)
	{
		boolean status = false;
		Connection con = null;
		PreparedStatement pst = null;
		String query = "UPDATE COURSES SET SP=?, FA=?, SECTIONS=? WHERE COURSE = ?";
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
			pst.setString(1, sp);
			pst.setString(2, fa);
			pst.setInt(3, sections);
			pst.setString(4, course);
			
			int rows = pst.executeUpdate();
			if(rows > 0)
			{
				status = true;
				for(int i = 1; i <= sections;i++)
				{
					Section sec = getSection(course, i);
					if(sec == null)
					{
						Section secNew = new Section();
						secNew.setCourse(course);
						secNew.setSection(i);
						secNew.setMondayFlag(1);
						secNew.setTuesdayFlag(0);
						secNew.setWednesdayFlag(1);
						secNew.setThursdayFlag(0);
						secNew.setFridayFlag(1);
						
						saveSection(secNew);
					}
				}
				deleteSections(course, sections);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			try {
				pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return status;
	}
	
	public static List<Section> getSections(String course)
	{
		List<Section> retList = new ArrayList<Section>();
		
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String courseTable = course.replaceAll(" ", "_");
		String query = "SELECT ID, COURSE, SECTION_NO, MONDAY_FLAG, TUESDAY_FLAG, "+
			"WEDNESDAY_FLAG, THURSDAY_FLAG, FRIDAY_FLAG, START_TIME, END_TIME FROM "+
			courseTable+" WHERE COURSE = ?";
		
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
			pst.setString(1, course);
			rs = pst.executeQuery();
			while(rs.next())
			{
				Section sec = new Section();
				sec.setId(rs.getInt(1));
				sec.setCourse(rs.getString(2));
				sec.setSection(rs.getInt(3));
				sec.setMondayFlag(rs.getInt(4));
				sec.setTuesdayFlag(rs.getInt(5));
				sec.setWednesdayFlag(rs.getInt(6));
				sec.setThursdayFlag(rs.getInt(7));
				sec.setFridayFlag(rs.getInt(8));
				sec.setStartTime(rs.getString(9));
				sec.setEndTime(rs.getString(10));
				
				System.out.println("sat = "+sec.getFridayFlag());
				
				retList.add(sec);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			try {
				pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return retList;
	}
	
	public static Section getSection(String course, int section)
	{
		Section sec = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String courseTable = course.replaceAll(" ", "_");
		String query = "SELECT ID, COURSE, SECTION_NO, MONDAY_FLAG, TUESDAY_FLAG, "+
			"WEDNESDAY_FLAG, THURSDAY_FLAG, FRIDAY_FLAG, START_TIME, END_TIME FROM "+courseTable+
			" WHERE COURSE = ? AND SECTION_NO = ?";
		
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
			pst.setString(1, course);
			pst.setInt(2, section);
			rs = pst.executeQuery();
			while(rs.next())
			{
				sec = new Section();
				sec.setId(rs.getInt(1));
				sec.setCourse(rs.getString(2));
				sec.setSection(rs.getInt(3));
				sec.setMondayFlag(rs.getInt(4));
				sec.setTuesdayFlag(rs.getInt(5));
				sec.setWednesdayFlag(rs.getInt(6));
				sec.setThursdayFlag(rs.getInt(7));
				sec.setFridayFlag(rs.getInt(8));
				sec.setStartTime(rs.getString(9));
				sec.setEndTime(rs.getString(10));

				System.out.println("fri = "+sec.getFridayFlag());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			try {
				pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return sec;
	}
	
	public static boolean saveSection(Section sec)
	{
		boolean status = false;
		Connection con = null;
		Statement st = null;
		PreparedStatement pst = null;
		String courseTable = sec.getCourse().replaceAll(" ", "_");
		String query = "INSERT INTO "+courseTable+" (COURSE, SECTION_NO, "+
			"MONDAY_FLAG, TUESDAY_FLAG, WEDNESDAY_FLAG, THURSDAY_FLAG, FRIDAY_FLAG)"+
			" VALUES (?,?,?,?,?,?,?)";
		try {
			con = DbConnectionUtil.getConnection();
			st = con.createStatement();
			String tableQuery = "CREATE TABLE "+courseTable
				+ "("
				+ "	ID INTEGER(10) AUTO_INCREMENT PRIMARY KEY,"
				+ "	COURSE VARCHAR(200),"
				+ "	SECTION_NO INTEGER(10),"
				+ "	START_TIME VARCHAR(20),"
				+ "	END_TIME VARCHAR(20),"
				+ "	MONDAY_FLAG INTEGER(1),"
				+ "	TUESDAY_FLAG INTEGER(1),"
				+ "	WEDNESDAY_FLAG INTEGER(1),"
				+ "	THURSDAY_FLAG INTEGER(1),"
				+ "	FRIDAY_FLAG INTEGER(1)"
				+ ");";
			try {
				st.execute(tableQuery);
			} catch (Exception e) {
			}
			
			pst = con.prepareStatement(query);
			pst.setString(1, sec.getCourse());
			pst.setInt(2, sec.getSection());
			pst.setInt(3, sec.getMondayFlag());
			pst.setInt(4, sec.getTuesdayFlag());
			pst.setInt(5, sec.getWednesdayFlag());
			pst.setInt(6, sec.getThursdayFlag());
			pst.setInt(7, sec.getFridayFlag());
			
			
			int rows = pst.executeUpdate();
			if(rows > 0)
			{
				status = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
			} catch (SQLException e1) {
			}
			
			try {
				con.close();
			} catch (SQLException e) {
			}
		}
		return status;
	}
	
	public static boolean updateSection(Section sec)
	{
		boolean status = false;
		Connection con = null;
		PreparedStatement pst = null;
		String courseTable = sec.getCourse().replaceAll(" ", "_");
		String query = "UPDATE "+courseTable+" SET MONDAY_FLAG = ?, "
			+ " TUESDAY_FLAG = ?, WEDNESDAY_FLAG = ?, THURSDAY_FLAG = ?, FRIDAY_FLAG = ?, "
			+ " START_TIME = ?, END_TIME = ? "
			+ " WHERE COURSE = ? AND SECTION_NO = ?";
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
			pst.setInt(1, sec.getMondayFlag());
			pst.setInt(2, sec.getTuesdayFlag());
			pst.setInt(3, sec.getWednesdayFlag());
			pst.setInt(4, sec.getThursdayFlag());
			pst.setInt(5, sec.getFridayFlag());
			pst.setString(6, sec.getStartTime());
			pst.setString(7, sec.getEndTime());
			pst.setString(8, sec.getCourse());
			pst.setInt(9, sec.getSection());
			
			int rows = pst.executeUpdate();
			if(rows > 0)
			{
				status = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return status;
	}
	
	public static boolean deleteSections(String course, int section)
	{
		boolean status = false;
		Connection con = null;
		PreparedStatement pst = null;
		String courseTable = course.replaceAll(" ", "_");
		String query = "DELETE FROM "+courseTable+" WHERE COURSE = ? AND SECTION_NO > ?";
		try {
			con = DbConnectionUtil.getConnection();
			pst = con.prepareStatement(query);
			pst.setString(1, course);
			pst.setInt(2, section);
			
			int rows = pst.executeUpdate();
			if(rows > 0)
			{
				status = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return status;
	}
	*/
}
