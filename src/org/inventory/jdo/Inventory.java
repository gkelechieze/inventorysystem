package org.inventory.jdo;

import java.util.Date;

public class Inventory {

	private int id;
	private int assetRequestId;
	private String requestedUserName;
	private String assetNumber;
	private String cpModelNo;
	private String serialNo;
	private String ownTag;
	private double totalCost;
	private String ccBuilding;
	private Date acquiredDate;
	private String available;
	private String purpose;
	private String src;
	private String description;
	private String purchaseAccounts;
	private String deptSub;
	private String restGroup;
	private String voucher;
	private String otherLocation;
	private String assignedTo;
	private String assetStatus;
	private int recordStatus;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAssetRequestId() {
		return assetRequestId;
	}
	public void setAssetRequestId(int assetRequestId) {
		this.assetRequestId = assetRequestId;
	}
	public String getRequestedUserName() {
		return requestedUserName;
	}
	public void setRequestedUserName(String requestedUserName) {
		this.requestedUserName = requestedUserName;
	}
	public String getAssetNumber() {
		return assetNumber;
	}
	public void setAssetNumber(String assetNumber) {
		this.assetNumber = assetNumber;
	}
	public String getCpModelNo() {
		return cpModelNo;
	}
	public void setCpModelNo(String cpModelNo) {
		this.cpModelNo = cpModelNo;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getOwnTag() {
		return ownTag;
	}
	public void setOwnTag(String ownTag) {
		this.ownTag = ownTag;
	}
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	public String getCcBuilding() {
		return ccBuilding;
	}
	public void setCcBuilding(String ccBuilding) {
		this.ccBuilding = ccBuilding;
	}
	public Date getAcquiredDate() {
		return acquiredDate;
	}
	public void setAcquiredDate(Date acquiredDate) {
		this.acquiredDate = acquiredDate;
	}
	public String getAvailable() {
		return available;
	}
	public void setAvailable(String available) {
		this.available = available;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPurchaseAccounts() {
		return purchaseAccounts;
	}
	public void setPurchaseAccounts(String purchaseAccounts) {
		this.purchaseAccounts = purchaseAccounts;
	}
	public String getDeptSub() {
		return deptSub;
	}
	public void setDeptSub(String deptSub) {
		this.deptSub = deptSub;
	}
	public String getRestGroup() {
		return restGroup;
	}
	public void setRestGroup(String restGroup) {
		this.restGroup = restGroup;
	}
	public String getVoucher() {
		return voucher;
	}
	public void setVoucher(String voucher) {
		this.voucher = voucher;
	}
	public String getOtherLocation() {
		return otherLocation;
	}
	public void setOtherLocation(String otherLocation) {
		this.otherLocation = otherLocation;
	}
	public String getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	public String getAssetStatus() {
		return assetStatus;
	}
	public void setAssetStatus(String assetStatus) {
		this.assetStatus = assetStatus;
	}
	public int getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}
	
}
