<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*, org.inventory.dao.*, org.inventory.jdo.*" %>

<%
	String username = request.getParameter("user_name");
	String password = request.getParameter("user_password");
	User usr = UserDao.validateUser(username, password);
	if( usr != null)
	{
			session.invalidate();
			session = request.getSession(true);
			System.out.println("valid user; "+session.isNew());
			session.setAttribute("username", usr.getUserName());
			session.setAttribute("usertype",usr.getUserType());
			session.removeAttribute("action");
			session.removeAttribute("message");
	}
	else
	{
	    	session.setAttribute("action", "error");
			session.setAttribute("message", "Invalid username or password. Please try again.");
	}
	response.sendRedirect("./");
%>